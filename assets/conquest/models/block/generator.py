import argparse

parser = argparse.ArgumentParser()

parser.add_argument('configFile')
args = parser.parse_args()

config_file = args.configFile


# returns [[input, output], pattern to replace, string to replace with]
def parse_config_file(config_file_name):
    state = 0           # 0 = nothing, 1 = input, 2 = pattern, 3 = replace, 4 = output files
    input_files = []
    pattern = None
    replace = None
    with open(config_file_name, "r") as file:
        for line in file:
            line = line.split('\n')[0]
            if line == "INPUT":
                state = 1
            elif line == "PATTERN":
                state = 2
            elif line == "REPLACE":
                state = 3
            else:
                if state == 0:
                    pass # ignore
                if state == 1:
                    infile, outfile = line.split()
                    input_files.append([infile, outfile])
                elif state == 2:
                    pattern = line
                elif state == 3:
                    replace = line
    if len(input_files) == 0 or pattern is None or replace is None:
        raise Exception("invalid configuration file")
    return [input_files, pattern, replace]


def replace_in_file(filename, pattern_to_replace, replace_with, outputname):
    content = None
    with open(filename, "r") as input_file:
        content = input_file.read().replace(pattern_to_replace, replace_with)
    with open(outputname, "w") as output_file:
        output_file.write(content)


def generate_models(configuration):
    for infile, outfile in configuration[0]:
        replace_in_file(infile, configuration[1], configuration[2], outfile)


generate_models(parse_config_file(config_file))
